package com.dgc.tag.repository;

import com.dgc.tag.domain.TagPrecedent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface TagPrecedentRepository extends JpaRepository<TagPrecedent, Long> {

    Set<TagPrecedent> findByTagFatherTag(String tag);

    void deleteByTagFatherIdTag(Long tagIdFather);

    Set<TagPrecedent> findByTagFatherIdTag (Long idTag);

    Set<TagPrecedent> findByTagSonIdTag(Long idTag);

}