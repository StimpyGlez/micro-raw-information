package com.dgc.tag.repository;

import com.dgc.tag.domain.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findByIdTag( long tagId);

    Tag findByTag(String tag);

    @Query("FROM Tag T ORDER BY T.tag ")
    List<Tag> getAllTags();

    Set<Tag> findByTagNotIn(String[] tag);

    @Query("SELECT T FROM Tag AS T WHERE T.idTag <> :idTag AND T.idTag <> (SELECT P.tagSon.idTag FROM TagPrecedent P WHERE P.tagFather.idTag = :idTag) ")
    Set<Tag> obtenerActividadesAsignar(@Param("idTag") Long idTag);

    @Query(" SELECT T FROM Tag AS T WHERE T.idTag <> :idTag AND T.idTag NOT IN ( SELECT P.tagSon.idTag FROM TagPrecedent P WHERE P.tagFather.idTag = :idTag)")
    Set<Tag> obtenerNoHijosNoOwner(@Param("idTag") Long idTag);

    /*
    @Query("SELECT T FROM Tag T WHERE T.tag = :tag AND T.idTag IN (SELECT DISTINCT P.idTag FROM TagPlanta P WHERE P.plantaId = :plantaId )")
    Tag obtenerPorTagAndPlantaId(@Param("tag") String tag, @Param("plantaId") Long plantaId);
    */

    Set<Tag> findByIdTypeComplianceAndIdActivity(Long idComplianceType, Long idActivity);

}