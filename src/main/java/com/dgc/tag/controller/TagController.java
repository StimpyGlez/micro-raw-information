package com.dgc.tag.controller;

import com.dgc.dto.ResponseDTO;
import com.dgc.dto.tag.TagInDTO;
import com.dgc.dto.tag.TagOutDTO;
import com.dgc.dto.tag.TagPrecedenteDTO;
import com.dgc.tag.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/tag")
@Transactional
public class TagController {

    @Autowired
    TagService tagService;

    @CrossOrigin(origins = "*")
    @PostMapping("/save")
    public TagInDTO saveTag(@RequestBody TagInDTO  tagInDTO) {
        return tagService.saveTag(tagInDTO);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/all")
    public List<TagInDTO> getAll(){
        return tagService.getAll();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{tag}")
    public TagInDTO getByTag(@PathVariable("tag") String tag){
        return tagService.getByTag(tag);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("obten/tag/id/{tagId}")
    public TagInDTO obtenTag(@PathVariable("tagId") Long idTag){
        return tagService.obtenTagPorId(idTag);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("getActividadesPrecedentes/{tagPadre}")
    public Set<TagInDTO> getActivitiesPrecedents(@PathVariable("tagFhater") String tagFather){
        return tagService.getActividadesPrecedentes(tagFather);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("agregarPrecedentes/{tag}/{tags}")
    public Set <TagPrecedenteDTO> addPrecedents(@PathVariable("tag") String tagPadre, @PathVariable("tags") String[] tags){
        return tagService.addPrecedents(tagPadre, tags);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/eliminarPrecedente")
    Set <TagPrecedenteDTO> deletePrecedent(@RequestBody TagPrecedenteDTO tagPrecedent) {
        return tagService.deletePrecedent( tagPrecedent );
    }

    @CrossOrigin(origins = "*")
    @GetMapping("tag/eliminar/{tagId}")
    ResponseDTO deleteTag(@PathVariable("tagId") Long tagId) {
        return tagService.deleteTag(tagId);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("tag/actividad/{maestroOpcionId}/{actividadId}")
    Set<TagOutDTO> tagsByActivity(@PathVariable("maestroOpcionId") Long maestroOpcionId, @PathVariable("idActivity") Long idActivity){
        return tagService.findByIdTypeComplianceAndIdActivity(maestroOpcionId, idActivity);
    }


}
