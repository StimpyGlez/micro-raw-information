package com.dgc.tag.multitenant.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;


@Slf4j
@Configuration
public class MultiTenantConfiguration {

    private final MultiTenantManager tenantManager;

    public MultiTenantConfiguration(MultiTenantManager tenantManager) {
        this.tenantManager = tenantManager;
        this.tenantManager.setTenantResolver(MultiTenantConfiguration::tenantResolver);
    }

    public static DataSourceProperties tenantResolver(String tenantId) {
        File[] files = Paths.get("tenants").toFile().listFiles();
        if (files == null) {
            String msg = "[!] Tenant property files not found at ./tenants folder!";
            log.error(msg);
            throw new RuntimeException(msg);
        }

        for (File propertyFile : files) {
            Properties tenantProperties = new Properties();
            try {
                tenantProperties.load(new FileInputStream(propertyFile));
            } catch (IOException e) {
                String msg = "[!] Could not read tenant property file at ./tenants folder!";
                log.error(msg);
                throw new RuntimeException(msg, e);
            }

            String id = tenantProperties.getProperty("id");
            if (tenantId.equals(id)) {
                DataSourceProperties properties = new DataSourceProperties();
                properties.setUrl(tenantProperties.getProperty("url"));
                properties.setUsername(tenantProperties.getProperty("username"));
                properties.setPassword(tenantProperties.getProperty("password"));
                return properties;
            }
        }
        String msg = "[!] Any tenant property files not found at ./tenants folder!";
        log.error(msg);
        throw new RuntimeException(msg);
    }


}
