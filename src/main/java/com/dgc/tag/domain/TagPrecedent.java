package com.dgc.tag.domain;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CAT_TAG_PRECEDENT")
public class TagPrecedent extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_TAG_PRECEDENT")
    private Long idTagPrecedent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TAG_FATHER")
    private Tag tagFather;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TAG_SON")
    private Tag tagSon;

}