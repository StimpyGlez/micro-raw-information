package com.dgc.tag.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CAT_TAG")
public class Tag extends BaseEntity {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column(name = "ID_TAG")
    private Long idTag;

    @Column(name = "TAG", unique = true)
    private String tag;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ID_TYPE_COMPLIANCE")
    private Long idTypeCompliance;

    @Column(name = "ID_AUTHORITY")
    private Long idAuthority;

    @Column(name = "ID_DELIVERY_PERIOD")
    private Long idDeliveryPeriod;

    @Column(name = "ID_DAYS_TYPE")
    private Long idDaysType;

    @Column(name = "ID_ACTIVITY")
    private Long idActivity;

    @Column(name = "ACTIVITY_CLASSIFICATION")
    private String classificationActivity;

    @Column(name = "ID_APPLICATION_TYPE")
    private Long idApplicationType;

    @Column(name = "LEGAL_REQUIREMENT")
    private String requisitoLegal;

    @Column(name = "ID_STATUS")
    private Long idStatus;

}
